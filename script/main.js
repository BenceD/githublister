var app = angular.module('githubrepolist', ['ngRoute']);

app.config(function($routeProvider, $locationProvider) {
	$routeProvider
	.when('/', {
		templateUrl: 'view/repos.html',
		controller: 'reposCtrl'
	})
	.when('/details/:user/:repo', {
		templateUrl: 'view/details.html',
		controller: 'detailsCtrl'
	});
});