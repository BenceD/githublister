app.service('api', ['$http', function ($http) {
	var domain = "https://api.github.com";
	return {
		issueList: function(param)
		{
			var req = {method: 'GET',url: domain+'/repos/'+param+'/issues'}; //format: username/repo
			return $http(req).then(function(res){
				return res.data;
			}, function(err){
				console.log(err);
			});
		},
		searchRepos: function(param){
			var req = {method: 'GET',url: domain+'/search/repositories?q='+param}; //search string
			return $http(req).then(function(res){
				return res.data.items;
			}, function(err){
				console.log(err);
			});
		},
		repoDetails: function(param){
			var req = {method: 'GET',url: domain+'/repos/'+param}; //format: username/repo
			return $http(req).then(function(res){
				return res.data;
			}, function(err){
				console.log(err);
			});
		}
	};
}]);