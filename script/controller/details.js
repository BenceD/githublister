app.controller('detailsCtrl', ['$scope', '$http', '$routeParams', 'api', function($scope, $http, $routeParams, api) {
	var repo = $routeParams.user+"/"+$routeParams.repo;
	$scope.loading = true;
	api.issueList(repo).then(function(res){
		$scope.loading = false;
		$scope.issuelist = res;
	});
	api.repoDetails(repo).then(function(res){
		$scope.repo = res;
	});
}]);