app.controller('reposCtrl', ['$scope', '$http', 'api', function($scope, $http, api) {
	$scope.loading = false;
	$scope.repos=[];
	
	$scope.runSearch = function(){
		$scope.repos=[];
		$scope.loading = true;
		api.searchRepos($scope.search).then(function(res){
			$scope.loading = false;
			$scope.repos=res;
		});
	}
}]);